<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<!-- Basic Page Needs 
========================================================= -->
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php bloginfo('name'); ?> <?php wp_title(' - ', true, 'left'); ?></title>

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php global $data; ?>

<!-- Mobile Specific Metas & Favicons
========================================================= -->
<?php if($data['check_mobilezoom'] == 0) { ?><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"><?php } ?>

<?php if($data['media_favicon'] != "") { ?><link rel="shortcut icon" href="<?php echo $data['media_favicon']; ?>"><?php } ?>

<?php if($data['media_favicon_iphone'] != "") { ?><link rel="apple-touch-icon" href="<?php echo $data['media_favicon_iphone']; ?>"><?php } ?>

<?php if($data['media_favicon_iphone_retina'] != "") { ?><link rel="apple-touch-icon" sizes="114x114" href="<?php echo $data['media_favicon_iphone_retina']; ?>"><?php } ?>

<?php if($data['media_favicon_ipad'] != "") { ?><link rel="apple-touch-icon" sizes="72x72" href="<?php echo $data['media_favicon_ipad']; ?>"><?php } ?>

<?php if($data['media_favicon_ipad_retina'] != "") { ?><link rel="apple-touch-icon" sizes="144x144" href="<?php echo $data['media_favicon_ipad_retina']; ?>"><?php } ?>

<!-- typekit + easydonate -->
<script src="https://use.typekit.net/mqm4kqj.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/framework/js/akin_donate.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/framework/fonts/mrdafoe.css" type="text/css" charset="utf-8" />

<!-- WordPress Stuff
========================================================= -->
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=184195104992428";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
    
		<div class="container">
			<div class="three columns">
            	<div class="akin_logo">
					<a href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/images/akin_logo.png" alt="<?php bloginfo('name'); ?>" class="logo_standard" /></a>
                </div>
                &nbsp;
			</div>
            
            <div class="ten columns">
				<div class="topmenu">
                    <ul>
                        <li><a href="<?php echo home_url(); ?>/">Home</a></li>
                        <!--<li><a href="/where-we-are/">Where Are We</a></li>-->
                        <li><a href="<?php echo home_url(); ?>/our-founder/">A message from our Founder</a></li>
                        <li class="no-right"><a href="http://www.facebook.com/AKINorg" target="_blank">AKIN Facebook</a></li>
                    </ul>
                </div>
                
                <div class="menuprincipal">
                	<ul>
                        <li><a href="<?php echo home_url(); ?>/about-akin/">About AKIN</a></li>
                        <li><a href="<?php echo home_url(); ?>/projects/">Student Projects</a></li>
                        <li><a href="<?php echo home_url(); ?>/media/">Media</a></li>
                        <li class="no-right"><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
                    </ul>
                </div>
			</div>
            
            <div class="three columns">
				<!-- DONATE -->
                <div class="donate-today contrast">
                <a href="<?php echo home_url(); ?>/donate/"><img src="<?php echo get_template_directory_uri(); ?>/images/donate_today.png" alt="Donate today to AKIN. Anything counts." class="logo_standard" /></a>
                </div>
			</div>
			
</div>