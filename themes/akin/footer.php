	<div class="clear"></div>
	
	<?php global $data; ?>
    
    <div id="copyright">
    <div class="container">
    	<div class="one column">
        	<img src="<?php echo get_template_directory_uri(); ?>/images/akin_footer.gif" alt="African Kids in Need" title="African Kids in Need" />
        </div>
        <div class="eleven columns">
			<div class="menuprincipal" style="text-align:left;">
                	<ul style="padding: 0;">
                        <li><a href="<?php echo home_url(); ?>/about-akin/">About AKIN</a></li>
                        <li><a href="<?php echo home_url(); ?>/projects/">Student Projects</a></li>
                        <li><a href="<?php echo home_url(); ?>/media/">Media</a></li>
                        <li class="no-right"><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
                    </ul>
                </div>
                <div class="topmenu" style="text-align: left !important; height: 50px;">
                    <ul>
                        <li style="padding-left: 0;"><a href="/">Home</a></li>
                        <!--<li><a href="/where-we-are/">Where Are We</a></li>-->
                        <li><a href="<?php echo home_url(); ?>/our-founder/">A message from our Founder</a></li>
                        <li class="no-right"><a href="http://www.facebook.com/AKINorg" target="_blank">AKIN Facebook</a></li>
                        
                    </ul>
                </div>
        </div>
        
        <div class="four columns" style="padding-top: 5px;">
       <a href="<?php echo home_url(); ?>/donate/"><img src="<?php echo get_template_directory_uri(); ?>/images/donate_today_footer.png" alt="Donate today to AKIN. Anything counts." class="logo_standard contrast"/></a>
        </div>
        
        <div class="clear"></div>
        <div class="sixteen columns">
        <div class="hr hr1" style="background-color: #4f322c; border-bottom: 0 !important; margin:7px 0px 7px 0px !important;"></div>
<div class="footer_copytext">African Kids In Need (EIN: 20-8511103) is a United States 501(c)3 nonprofit charitable organization and a registered NGO in Kenya founded by Paul Miller, the organization's President, in 2007.<br />A.K.I.N. is headquartered in Los Angeles and has a field office in Nairobi, Kenya. Website design and development kindly donated by <a href="http://talaguim.com/" target="_blank">talaguim</a>.</div>
        </div>
	</div>
    
    </div>

	<div id="back-to-top"><a href="#"><?php _e( 'Voltar ao topo', 'minti' ) ?></a></div>

	<?php if($data['textarea_trackingcode'] != '') { echo $data['textarea_trackingcode']; } ?>
	
	<?php wp_footer(); ?>
	
</body>

</html>
