<?php

/*
Plugin Name: Custom Project Widget
Plugin URI: http://twitter.com/hellominti/
Description: A simple but powerful widget to display Project Items.
Version: 1.00
Author: minti
Author URI: http://twitter.com/hellominti/
*/

class widget_project extends WP_Widget { 
	
	// Widget Settings
	function widget_project() {
		$widget_ops = array('description' => __('Display your latest Project') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'project' );
		$this->WP_Widget( 'project', __('minti.Project'), $widget_ops, $control_ops );
	}
	
	// Widget Output
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$number = $instance['number'];
		
		echo $before_widget;

		if($title) {
			echo $before_title . $title . $after_title;
		}
		?>
		<div class="recent-works-items clearfix">
		<?php
		$args = array(
			'post_type' => 'project',
			'posts_per_page' => $number
		);
		$project = new WP_Query($args);
		if($project->have_posts()):
		?>
		<?php while($project->have_posts()): $project->the_post(); ?>
		<div class="project-widget-item">
            <?php if (has_post_thumbnail()) { ?>
            	<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" class="project-pic"><?php the_post_thumbnail( 'mini' ); ?></a>
            <?php } ?>
       </div>
		<?php endwhile; endif; ?>
		</div>

		<?php echo $after_widget;
	}
	
	// Update
	function update( $new_instance, $old_instance ) {  
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		
		return $instance;
	}
	
	// Backend Form
	function form($instance) {
		
		$defaults = array('title' => 'Latest Project', 'number' => 1);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('number'); ?>">Number of items to show:</label>
			<input class="widefat" style="width: 30px;" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $instance['number']; ?>" />
		</p>
	<?php
	}
}

// Add Widget
function widget_project_init() {
	register_widget('widget_project');
}
add_action('widgets_init', 'widget_project_init');

?>