<div class="post clearfix">

	<?php if ( has_post_thumbnail() ) { ?>
	<div class="post-image">
		<!--<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" title="<?php the_title(); ?>" rel="bookmark">
			<?php the_post_thumbnail('standard'); ?>
		</a>--><?php the_post_thumbnail('standard'); ?>
	</div>
	<?php } ?>
	
	<div class="post-content clearfix">
		<div class="post-title">
			<h2 style="margin-bottom: 0 !important; margin-left: -55px;"><?php the_title(); ?></h2>
		</div>
		<div class="post-meta"><?php get_template_part( 'framework/inc/meta' ); ?></div>
        <div class="clear"></div>
		<div class="post-excerpt"><?php the_content(); ?></div>		
		<div class="post-tags clearfix"><?php the_tags( '', '', ''); ?></div>
	</div>

</div>

