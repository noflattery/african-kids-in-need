<?php 
	global $data;
	global $blogtype;
	if ($blogtype == 'medium') {
		$thumbnail_size = 'blog-medium';
	}
	if ($blogtype == 'large') {
		$thumbnail_size = 'standard';
	}
?>

<div class="post clearfix">
	
	<div class="post-content">
		<div class="post-title post-listings">
			<h2><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'minti'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		</div>
        <div class="post-meta"><?php get_template_part( 'framework/inc/meta' ); ?></div>
            <div class="gap" style="height: 20px;"></div><div class="clear"></div>
		<div class="post-excerpt"><?php the_excerpt(); ?></div>
        <div class="clear"></div>
	<div class="hr hr2" style="margin:30px 0px 30px 0px !important;"></div>
	</div>
	
	

</div>

