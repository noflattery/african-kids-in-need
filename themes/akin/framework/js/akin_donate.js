// Custom Coded PayPal Donation Change
// by Pedro Rosa
// peucaio@gmail.com

	function changeDonate(type,which,amount) {
	
		var divname = document.getElementById("donate_"+type+"_"+which); // ex: donate_monthly_50
		
		// muda classe clicada
		divname.className = "donate_cover_clicked";
		
		// muda outras classes do mesmo type para não clicadas
		for (var i=1;i<5;i++)
		{ 
			if (i != which) {
				document.getElementById("donate_"+type+"_"+i).className = "donate_cover";
			}
		}
		
		// altera valor adicionado no campo principal
		oFormObject = document.forms['form_'+type];
		
		if (type == 'monthly') {
			oFormObject.elements["a3"].value = amount;
		} else {
			oFormObject.elements["amount"].value = amount;	
		}
		
	}