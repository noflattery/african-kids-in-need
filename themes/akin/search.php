<?php get_header(); ?>

<div class="title_bg_menor">
<div class="container">
<div class="twelve columns">
<h1 class="pagetitle">Search results <span style="font-size: 24px;">/</span> <span class="akinbrown"><?php the_search_query(); ?></span></h1>
</div>
<div class="four columns" style="text-align: right;">

</div>
</div>
</div>


<div id="page-wrap" class="container">
	
	<div id="content" class="<?php echo get_post_meta( get_option('page_for_posts'), 'minti_sidebar', true ); ?> twelve columns search">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
			<?php $blogtype = 'large'; ?>
			
			<?php get_template_part( 'framework/inc/post-format/content', get_post_format() ); ?>
			
			
			
			
	
		<?php endwhile; ?>
		
	
		<?php get_template_part( 'framework/inc/nav' ); ?>
	
		<?php else : ?>
	
			<h2><?php _e('Not Found', 'minti') ?></h2>
	
		<?php endif; ?>
	
	</div>

<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>
