<?php
/*
 Template Name: Home Page (Full Width)
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
			
			<div id="content">
				<div class="home_slider">
					<div class="home_slider_welcome">
						<a href="/donate">
							<div class="donate-mobile">
								Donate Today
							</div>
						</a>	
					</div>
				</div>

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-3of3 d-7of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">


								<section class="entry-content cf" itemprop="articleBody">
									<div class="container">
										<div class="project-block">
											<div class="m-all t-1of4 d-1of4 home-article-image">
												<?php 

												$image = get_field('article_image');

												if( !empty($image) ): ?>

													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="projects-image" />

												<?php endif; ?>
											</div>
										
											<div class="m-all t-3of4 d-3of4 cf">
												<?php 

												$content = get_field('article_content');

												if( !empty($content) ): ?>

													<?php echo $content; ?>

												<?php endif; ?>

											</div>
										</div>
									</div>
								</section>


								<footer class="article-footer">

                  <?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>

								</footer>

								<?php comments_template(); ?>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

				</div>

			</div>


<?php get_footer(); ?>
