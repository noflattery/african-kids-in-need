<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input type="hidden" name="cmd" value="_s-xclick" /><input type="hidden" name="hosted_button_id" value="HXFWQWJ4JZPS4" />
	<div class="page_sep"></div>
	<a name="sponsor"></a>
	<div class="container" id="page-wrap">
	<h3 class="akingreen">I want to sponsor a child for one year</h3>
	<h4>Our children constantly asks if they have been funded. Supporting one child for one year guarantees that he or she will continue their education without interruption, so that they may focus on learning and succeeding in school.</h4>
	<div class="gap" style="height: 15px;"></div>
	<div class="d-1of3">
	<div class="donate_cover">
	<div class="donateprice akingreen"><span>$</span>1200<span class="donateslash">/year</span></div>
	<strong>One full year of studies</strong>
	Simply amazing. With this, you supply everything an AKIN student needs for one year.
	</div>
	</div>
	<div class="d-2of3 donation-usage">
	<h2>Your donation will be used in the following manner:</h2>
	<div class="hr hr2" style="margin: 15px 0px 15px 0px !important;"></div>
	<div class="clear"></div>
	<div class="d-1of3">
	<div style="width: 95%;">
		<div class="akingreen" style="margin-bottom: 15px;"><b>$600 per year</b></div>
		<strong>School Fees</strong>
		</div>
	</div>
	<div class="d-1of3">
	<div style="width: 95%;">
	<div class="akingreen" style="margin-bottom: 15px;"><b>$400 per year</b>
	or $24 per month</div>
	<strong>Clothing + Personal Effects</strong>
	Uniforms and shoes, toiletries, soaps, sanitary products.</div>
	</div>
	<div class="d-1of3 last">
	<div style="width: 95%;">
	<div class="akingreen" style="margin-bottom: 15px;"><b>$200 per year</b>
	or $10 per month</div>
	<b>School Supplies</b>
	Text books, pencils.</div>
	</div>
	</div>
	<div class="clear"></div>
	<div class="" style="margin: 20px 0;"><input class="donate_sponsor_submit_button" type="submit" name="submit" value="Donate now via Paypal" /></div>
	<div class="">
	<div class="donate_tax_deductible">We accept all credit cards. Every donation is tax deductible.
	<img title="We accept all credit cards." alt="We accept all credit cards." src="http://www.africankidsinneed.org/wp-content/themes/akin/images/paypal_creditcards.png" /></div>
	</div>
	</div>
	<div class="clear"></div>
</form>
<div class="page_sep"></div>

<div class="container">
	<form id="form_onetime" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="form_onetime"><input type="hidden" name="cmd" value="_donations" /><input type="hidden" name="business" value="BHV9XDWR9E8L6" /><input type="hidden" name="lc" value="US" /><input type="hidden" name="item_name" value="African Kids In Need - One Time Donation" /><input type="hidden" name="item_number" value="2" /><input type="hidden" name="no_note" value="1" /><input type="hidden" name="no_shipping" value="2" /><input type="hidden" name="src" value="1" /><input type="hidden" name="srt" value="0" /><input type="hidden" name="currency_code" value="USD" /><input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHosted" />
		<div class="page_sep"></div>
		<a name="onetime"></a>
		<div class="container" id="page-wrap">
		<h3 class="akingreen">I want to donate another amount</h3>
		<h4>No matter how much you're willing to donate, each dollar counts. Our priorities are first to pay school fees, then the cost of their books and personal effects, and finally the administration.</h4>
		<div class="gap" style="height: 15px;"></div>
		<div class="d-1of4">
		<div class="donate_cover" id="donate_onetime_1" onclick="changeDonate('onetime',1,800);">
		<div class="donateprice akingreen"><span>$</span>800</div>
		<strong>School Fees for One Year</strong>
		</div>
		</div>
		<div class="d-1of4">
		<div class="donate_cover" id="donate_onetime_2" onclick="changeDonate('onetime',2,400);">
		<div class="donateprice akingreen"><span>$</span>400</div>
		<strong>Personal Effects and Text Books</strong></div>
		</div>
		<div class="d-1of4" style="display: none;">
		<div class="donate_cover" id="donate_onetime_3" onclick="changeDonate('onetime',3,200);">
		<div class="donateprice akingreen"><span>$ </span>200</div>
		<b>6-month School Supplies</b>
		Text books, pencils, rubbers, papers, pens and more, for half an year.</div>
		</div>
		<div class="d-1of4 donation-usage" style="display: none;">
		<div class="donate_cover" id="donate_onetime_4" onclick="changeDonate('onetime',4,12);">
		<div class="donateprice akingreen"><span>$</span>12</div>
		<b>1-month Clothing</b>
		Minor donations still helps us a lot. With this, a child gets clothing or personal goods for one month.</div>
		</div>
		<div class="clear"></div>
		<div class="gap" style="height: 20px;"></div>
		<div class="d-2of3" style="margin-bottom: 10px;">
			<div class="donate_phrase">
				<div class="donate_phrase_left" style="margin-left: 25px;">I want to donate $</div>
				<div class="donate_input_left"><input class="donate_a3" id="amount" type="text" maxlength="15" name="amount" value=""/></div>
				<div class="donate_phrase_left">dollars to AKIN.</div>
				<div style="padding-right: 20px;">
					<img title="You can also type any amount." alt="You can also type any amount." src="http://www.africankidsinneed.org/wp-content/themes/akin/images/donate_typeanyamount.gif" />
				</div>
			</div>
		</div>

		<div class="d-1of3 last">
			<input class="donate_submit_button" type="submit" name="submit" value="Donate now via Paypal" />
			<div class="donate_tax_deductible">We accept all credit cards. Every donation is tax deductible.
				<img title="We accept all credit cards." alt="We accept all credit cards." src="http://www.africankidsinneed.org/wp-content/themes/akin/images/paypal_creditcards.png" />
			</div>
		</div>
	</form>
</div>
<div class="page_sep"></div>
<div class="container" style="margin-top: 25px;">
	<div class="m-all t-1of4 d-1of4">
		<a href="http://www.africankidsinneed.org/wp-content/uploads/2013/08/anppcan.gif"><img class="alignnone size-full wp-image-242" alt="AKIN and ANPPCAN" src="http://www.africankidsinneed.org/wp-content/uploads/2013/08/anppcan.gif" width="212" height="103" /></a>
	</div>
	<div class="m-all t-3of4 d-3of4">
			<div class="m-all t-2of3">
				<b>A.K.I.N.</b><br />
				<p>
					137 North Larchmont Blvd
					Box 140 - Los Angeles, CA 90004
					United States of America
				</p>
			</div>

		<?php the_content(); ?>

	</div>
</div>

<?php?>