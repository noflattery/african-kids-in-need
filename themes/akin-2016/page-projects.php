<?php
/*
 Template Name: Projects Page (Full Width)
*/
?>

<?php get_header(); ?>
			
			
			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog" style="margin-top: 50px;">
						
							<?php $title = get_field('first_title'); ?>
							<h4><?php echo $title; ?></h4>
							<div class="project-block">

								<div class="m-all t-1of4 d-1of4">
									<?php 

									$image = get_field('first_image');

									if( !empty($image) ): ?>

										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="projects-image" />

									<?php endif; ?>
								</div>
							
								<div class="m-all t-3of4 d-3of4 cf">
									<?php 

									$paragraph = get_field('first_paragraph');

									if( !empty($paragraph) ): ?>

										<p><?php echo $paragraph; ?></p>

									<?php endif; ?>

								</div>
							</div>

							<?php $title = get_field('second_title'); ?>
							<h4><?php echo $title; ?></h4>
							<div class="project-block">
								<div class="m-all t-1of4 d-1of4">
									<?php 
									
									$image = get_field('second_image');

									if( !empty($image) ): ?>

										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="projects-image" />

									<?php endif; ?>
								</div>

								<div class="m-all t-3of4 d-3of4 cf">
									<?php 

									$paragraph = get_field('second_paragraph');

									if( !empty($paragraph) ): ?>

										<p><?php echo $paragraph; ?></p>

									<?php endif; ?>

								</div>
							</div>


							<?php $title = get_field('third_title'); ?>
							<h4><?php echo $title; ?></h4>							
							<div class="project-block">

								<div class="m-all t-1of4 d-1of4">
									<?php 
									
									$image = get_field('third_image');

									if( !empty($image) ): ?>

										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="projects-image" />

									<?php endif; ?>
								</div>

								<div class="m-all t-3of4 d-3of4 cf">
									<?php 

									$paragraph = get_field('third_paragraph');

									if( !empty($paragraph) ): ?>

										<p><?php echo $paragraph; ?></p>

									<?php endif; ?>

								</div>
							</div>
						</main>

				</div>

			</div>


<?php get_footer(); ?>
