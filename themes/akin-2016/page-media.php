<?php
/*
 Template Name: Media Page (Full Width)
*/
?>

<?php get_header(); ?>
			
			<div class="title_bg">
				<div class="container wrap">
					<div class="m-all t-1of2 d-1of2">
						<?php the_field('left_copy'); ?>
					</div>
					<div class="m-all t-1of2 d-1of2">
						<?php 
						$image = get_field('right_image');

						if( !empty($image) ): ?>

							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="right-media-img" />

						<?php endif; ?>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-3of3 d-7of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<?php include (TEMPLATEPATH . '/includes/video-gallery.php' ); ?>
						</main>

				</div>

			</div>


<?php get_footer(); ?>
