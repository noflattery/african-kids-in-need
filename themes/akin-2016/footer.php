			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>

				    <div id="copyright">
					    <div class="container wrap">
					    	<div class="footer-logo">
					        	<img src="<?php echo get_template_directory_uri(); ?>/library/images/akin_footer.gif" alt="African Kids in Need" title="African Kids in Need" />
					        </div>
					        <div class="footer-menu">
								<div class="footer-links">
				                	<ul style="padding: 0;">
				                        <li><a href="<?php echo home_url(); ?>/about-akin/">About AKIN</a></li>
				                        <li><a href="<?php echo home_url(); ?>/projects/">Student Projects</a></li>
				                        <li><a href="<?php echo home_url(); ?>/media/">Media</a></li>
				                        <li class="no-right"><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
				                    </ul>
				                </div>
				                <div class="small-menu">
				                    <ul>
				                        <li style="padding-left: 0;"><a href="/">Home</a></li>
				                        <!--<li><a href="/where-we-are/">Where Are We</a></li>-->
				                        <li><a href="<?php echo home_url(); ?>/our-founder/">A message from our Founder</a></li>
				                        <li class="no-right"><a href="http://www.facebook.com/AKINorg" target="_blank">AKIN Facebook</a></li>
				                        
				                    </ul>
				                </div>
					        </div>
					        
					        <div class="footer-donate" style="padding-top: 5px;">
					       		<a href="<?php echo home_url(); ?>/donate/"><img src="<?php echo get_template_directory_uri(); ?>/library/images/donate_today_footer.png" alt="Donate today to AKIN. Anything counts." class="logo_standard contrast"/></a>
					        </div>
					        
					        <div class="clear"></div>
					        <div class="footnote">
					        	<div class="hr hr1" style="background-color: #4f322c; border-bottom: 0 !important; margin:7px 0px 7px 0px !important;"></div>
								<div class="footer_copytext">African Kids In Need (EIN: 20-8511103) is a United States 501(c)3 nonprofit charitable organization and a registered NGO in Kenya founded by Paul Miller, the organization's President, in 2007.<br />A.K.I.N. is headquartered in Los Angeles and has a field office in Nairobi, Kenya. Website design and development kindly donated by <a href="http://talaguim.com/" target="_blank">talaguim</a>.</div>
					        </div>
						</div>
					    
					</div>

					<div id="back-to-top"><a href="#"><?php _e( 'Voltar ao topo', 'minti' ) ?></a></div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
